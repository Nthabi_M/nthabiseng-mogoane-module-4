import 'package:flutter/material.dart';

import 'package:app/2pages/mainScreen.dart';
import 'package:app/2pages/MyYear.dart';
import 'package:app/auth/Login.dart';
import 'package:app/auth/Registration.dart';
import 'package:app/home/homeScreen.dart';
import 'package:app/profile/UserProfileEdit.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: "/",
      routes: {
        "/": (context) => const Login(),
        "/register": (context) => const Registration(),
        "/home": (context) => const homeScreen(),
        "/about": (context) => const MainScreen(),
        "/year": (context) => const MyYear(),
        "/profile": (context) => const UserProfileEdit()
      },
    );
  }
}
