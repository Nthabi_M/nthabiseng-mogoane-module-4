import 'package:flutter/material.dart';

class Year extends StatelessWidget {
  const Year({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("MyYear"),
        backgroundColor: Colors.pink,
      ),
      body: const Center(
          child: Text(
        "My For MTNApp Academy year is 2022",
        style:
            TextStyle(color: Color.fromARGB(255, 206, 22, 108), fontSize: 20),
      )),
    );
  }
}
